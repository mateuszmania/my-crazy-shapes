package wwsis.mono.app.shapes;

import java.awt.Color;
import java.awt.Graphics2D;

public abstract class RandomShape{

	private Color color;
	private int x;
	private int y;
	private int width;
	private int height;
	private int angle;

	public RandomShape(int x, int y, int width, int height, Color color, int angle) {
		this.color = color;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.angle = angle;
	}

	public RandomShape(int x, int y, int width, Color color, int angle) {
		this.color = color;
		this.x = x;
		this.y = y;
		this.width = width;
		this.angle = angle;
	}

	public Color getColor() {
		return color;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getAngle() {
		return angle;
	}

	public abstract void draw(Graphics2D g2d);

}

