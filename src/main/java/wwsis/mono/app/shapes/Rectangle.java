package wwsis.mono.app.shapes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class Rectangle extends RandomShape {

	public Rectangle(int x, int y, int width, int height, Color color, int angle) {
		super(x, y, width, height, color, angle);
	}

	@Override
	public void draw(Graphics2D g2d) {
		g2d.setColor(getColor());

		AffineTransform transform = new AffineTransform();
		transform.rotate(getAngle(), getX() + getWidth() / 2, getY() + getHeight() / 2);
		AffineTransform old = g2d.getTransform();
		g2d.transform(transform);

		g2d.fillRect(getX(), getY(), getWidth(), getHeight());
		g2d.drawRect(getX(), getY(), getWidth(), getHeight());

		g2d.setTransform(old);
	}
}
