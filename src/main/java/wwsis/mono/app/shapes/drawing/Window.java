package wwsis.mono.app.shapes.drawing;

import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

public class Window {

	static DrawShapes jPanel = new DrawShapes();

	public void open() {
		final JFrame jFrame = new JFrame();
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setSize(800, 600);
		jFrame.setLayout(null);
		jFrame.setVisible(true);

		MyList listModel = new MyList();

		Button bLoadJar = new Button("Pobierz");
		bLoadJar.setBounds(10, 530, 150, 30);
		bLoadJar.setBackground(Color.BLACK);
		bLoadJar.setForeground(Color.WHITE);

		bLoadJar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					listModel.addToList();
				} catch (SecurityException e1) {
					e1.printStackTrace();
				}
			}
		});

		jFrame.add(listModel);
		jFrame.add(jPanel);
		jFrame.add(bLoadJar);
		jFrame.setVisible(true);
		jFrame.setResizable(false);

	}
}
