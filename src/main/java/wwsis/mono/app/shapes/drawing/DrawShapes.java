package wwsis.mono.app.shapes.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import wwsis.mono.app.shapes.Circle;
import wwsis.mono.app.shapes.RandomShape;
import wwsis.mono.app.shapes.Rectangle;

public class DrawShapes extends JPanel {

	private static final long serialVersionUID = 1L;
	ArrayList<RandomShape> shapes = new ArrayList<RandomShape>(100);
	Random random = new Random();

	public DrawShapes() {
		setBackground(new Color(255, 243, 124));
		setBounds(170, 10, 610, 550);
		setBorder(new LineBorder(new Color(25, 83, 249), 5));
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		for (RandomShape shape : shapes) {
			shape.draw(g2d);
		}
	}

	public ArrayList<RandomShape> chooseShape(String whichShape)
			throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, ClassNotFoundException, IOException {

		int r = random.nextInt(256);
		int g = random.nextInt(256);
		int b = random.nextInt(256);
		Color color = new Color(r, g, b);

		int x = 10+random.nextInt(450);
		int y = 10+random.nextInt(390);

		int width = 150 - random.nextInt(100);
		int height = 150 - random.nextInt(100);

		int angle = random.nextInt(360);		

		if (whichShape == "Circle") {
			Circle circ = new Circle(x, y, width, height, color, angle);
			shapes.add(circ);
			System.out.println("Circle:" + x + " " + y + " " + width + " " + height + " " + angle);
			repaint();
		} else if (whichShape == "Rectangle") {
			Rectangle rect = new Rectangle(x, y, width, height, color, angle);	
			shapes.add(rect);
			System.out.println("Rect:" + x + " " + y + " " + width + " " + height + " " + angle);
			repaint();
		} else if (whichShape != "Circle" && whichShape != "Rectangle") {

			for (int i = 0; i < JarLoader.load().size(); i++) {
				String loadedFiguresName = JarLoader.load().get(i).getName().substring(22,
						JarLoader.load().get(i).getName().length());
				System.out.println(JarLoader.load() + "JarLoaderLoad");
				System.out.println(loadedFiguresName + "LoadedFiguresName");
				System.out.println(whichShape + "WhichShape");

				if (whichShape.equals(loadedFiguresName)) {
					System.out.println(loadedFiguresName + "Loaded String 2");
					System.out.println(JarLoader.load().get(i) + " MM");
					Constructor<? extends RandomShape> c = JarLoader.load().get(i).getDeclaredConstructor(int.class,
							int.class, int.class, int.class, Color.class, int.class);
					RandomShape o = c.newInstance(x, y, width, height, color, angle);
					shapes.add(o);
					repaint();
				} else {
					System.out.println("Next iteration");
					;
				}
			}
		}

		System.out.println("Shapes: " + shapes);
		return shapes;
	}
}