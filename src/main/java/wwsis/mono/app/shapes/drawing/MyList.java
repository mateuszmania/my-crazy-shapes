package wwsis.mono.app.shapes.drawing;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;

public class MyList extends JPanel {

	private static final long serialVersionUID = 1L;

	ArrayList<String> shapes = new ArrayList<String>(100);
	DefaultListModel<String> modelShapes = new DefaultListModel<>();
	JList<String> dataList = new JList<>(modelShapes);

	public MyList() {
		setLayout(null);
		setBounds(10, 10, 150, 500);
		setBackground(Color.yellow);
		setBorder(new LineBorder(new Color(25, 83, 249), 5));
		shapes.add("Circle");
		shapes.add("Rectangle");

		for (int i = 0; i < shapes.size(); i++) {
			modelShapes.addElement(shapes.get(i));
		}

		System.out.println(modelShapes + "MM");

		add(dataList);
		dataList.setBounds(10, 10, 130, 400);
		dataList.setBackground(Color.yellow);
		dataList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		dataList.setSelectedIndex(-1);
		setVisible(true);

		MouseListener mouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 1) {
					String selectedItem = (String) dataList.getSelectedValue();
					try {
						Window.jPanel.chooseShape(selectedItem);
					} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
							| IllegalArgumentException | InvocationTargetException e1) {
						e1.printStackTrace();
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		};
		dataList.addMouseListener(mouseListener);
	}

	public DefaultListModel<String> addToList() {
		String pathToJar = "src\\main\\other\\extra_shapes.jar";
		JarInputStream jarInputFile;
		try {
			jarInputFile = new JarInputStream(new FileInputStream(pathToJar));
			JarEntry jarEntry;
			while (true) {
				jarEntry = jarInputFile.getNextJarEntry();
				if (jarEntry == null) {
					break;
				}
				if (jarEntry.getName().endsWith(".class")) {
					modelShapes.addElement(
							jarEntry.getName().substring(22, jarEntry.getName().length() - 6).replaceAll("/", "\\."));
				}
				repaint();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return modelShapes;
	}

}
