package wwsis.mono.app.shapes.drawing;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import wwsis.mono.app.shapes.RandomShape;

public class JarLoader extends ClassLoader {

	public static ArrayList<Class<? extends RandomShape>> load()
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException {

		try {

			ArrayList<Class<? extends RandomShape>> loadedFigures = new ArrayList<Class<? extends RandomShape>>();

//			String pathToJar = "C:\\Users\\User\\Desktop\\triangle7.jar";
			String pathToJar = "src\\main\\other\\extra_shapes.jar";
			JarFile jarFile = new JarFile(pathToJar);

			Enumeration<JarEntry> e = jarFile.entries();

			URL[] urls = { new URL("jar", "", "file:" + pathToJar + "!/") };
			URLClassLoader cl = URLClassLoader.newInstance(urls);

			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				if (je.isDirectory() || !je.getName().endsWith(".class")) {
					continue;
				}
				String className = je.getName().substring(0, je.getName().length() - 6);
				className = className.replace('/', '.');
				Class<? extends RandomShape> clazz = (Class<? extends RandomShape>) cl.loadClass(className);
				loadedFigures.add(clazz);
			}
			System.out.println(loadedFigures + " XXX");
			return loadedFigures;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}
}
